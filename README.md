#Suggestion Search View

How to use?
-----------

**Step 1:** Add Menu Item

```xml
<item
    android:id="@+id/menu_search"
    android:title="@string/menu_search"
    app:actionViewClass="com.odoo.dpr.suggestionsearchview.SuggestionSearchView"
    app:showAsAction="always" />
```

**Step 2:** Bind ``SuggestionSearchView`` object

```java
@Override
public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.activity_main, menu);
    bindSearchView((SuggestionSearchView) MenuItemCompat.getActionView(menu.findItem(R.id.menu_search)));
    return super.onCreateOptionsMenu(menu);
}

private void bindSearchView(SuggestionSearchView searchView) {
}
```

**Step 3:** Add event listener 

```java
private void bindSearchView(SuggestionSearchView searchView) {
    // Setting listeners first
    searchView.setSearchViewActionListener(this);
}
```

will implement 2 methods,

```java
@Override
public Cursor OnSearchTextChange(String query) {
    return null;
}

@Override
public boolean OnSuggestionItemClick(int position, Cursor item) {
    return false;
}
```

``OnSearchTextChange`` will be triggered when user put input in search view. Create ``Cursor`` result based on query term

``OnSuggestionItemClick`` will be triggered when user click any on suggestion item

**Step 4:** Add custom view and bind them

```java
private void bindSearchView(SuggestionSearchView searchView) {
    // Setting listeners first
    searchView.setSearchViewActionListener(this);

    // For binding custom views.
    // From is a key names and to is view id in which to set value
    // SimpleCursorAdapter like view
    String[] from = {"name", "desc"};
    int[] to = {R.id.searchTitle, R.id.searchSubText};
    searchView.setAdapterItems(from, to, R.layout.search_suggestion_item);
}
```

That's it !

Watch demo : https://youtu.be/9mfZs2KFKww

&copy; Dharmang Soni