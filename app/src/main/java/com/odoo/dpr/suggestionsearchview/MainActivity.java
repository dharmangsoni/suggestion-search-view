package com.odoo.dpr.suggestionsearchview;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SuggestionSearchView.SearchViewActionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        bindSearchView((SuggestionSearchView) MenuItemCompat.getActionView(menu.findItem(R.id.menu_search)));
        return super.onCreateOptionsMenu(menu);
    }

    private void bindSearchView(SuggestionSearchView searchView) {
        // Setting listeners first
        searchView.setSearchViewActionListener(this);

        // For binding custom views.
        // From is a key names and to is view id in which to set value
        // SimpleCursorAdapter like view
        String[] from = {"name", "desc"};
        int[] to = {R.id.searchTitle, R.id.searchSubText};
        searchView.setAdapterItems(from, to, R.layout.search_suggestion_item);
    }


    /*
     * When user type any text in search view, You will get callback.
     * Just filter your data and create one cursor with values and return that.
     */
    @Override
    public Cursor OnSearchTextChange(String query) {
        final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "name", "desc"});
        for (int i = 1; i <= 5; i++) {
            c.addRow(new Object[]{i, query + " " + i, query + " desc " + i});
        }
        return c;
    }

    /**
     * When user click on your search result suggestion,
     * You will get the Cursor item that user have clicked.
     */
    @Override
    public boolean OnSuggestionItemClick(int position, Cursor item) {
        Toast.makeText(MainActivity.this, "Item Clicked at index  " + position, Toast.LENGTH_LONG).show();
        return true;
    }
}
